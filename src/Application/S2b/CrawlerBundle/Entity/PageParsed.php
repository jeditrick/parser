<?php

namespace Application\S2b\CrawlerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use S2b\CrawlerBundle\Entity\BasePageParsed;

/**
 * PageParsed
 *
 * @ORM\Table(name="s2b_crawler_page_parsed")
 * @ORM\Entity(repositoryClass="Application\S2b\CrawlerBundle\Entity\PageParsedRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class PageParsed //extends BasePageParsed
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="S2b\CrawlerBundle\Entity\Page", inversedBy="parsed")
     * @ORM\JoinColumn(name="page_id", referencedColumnName="id")
     **/
    protected $page;


    /**
     * @var string
     *
     * @ORM\Column(name="price_per_one", type="decimal", nullable=true)
     */
    protected $price_per_one;

    /**
     * @var string
     *
     * @ORM\Column(name="count_in_box", type="integer", nullable=true)
     */
    protected $count_in_box;


    /**
     * @var string
     *
     * @ORM\Column(name="size", type="string", length = 256)
     */
    protected $size;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length = 256)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal", nullable=true)
     */
    protected $price;

    /**
     * @var string
     *
     * @ORM\Column(name="photo_url", type="string", length=256)
     */
    protected $photo_url;

    /**
     * @var string
     *
     * @ORM\Column(name="sku", type="string", length=256)
     */
    protected $sku;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="downloaded_at", type="datetime", nullable=true)
     */
    protected $downloadedAt;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set price_per_one
     *
     * @param string $pricePerOne
     * @return PageParsed
     */
    public function setPricePerOne($pricePerOne)
    {
        $this->price_per_one = $pricePerOne;

        return $this;
    }

    /**
     * Get price_per_one
     *
     * @return string
     */
    public function getPricePerOne()
    {
        return $this->price_per_one;
    }

    /**
     * Set count_in_box
     *
     * @param integer $countInBox
     * @return PageParsed
     */
    public function setCountInBox($countInBox)
    {
        $this->count_in_box = $countInBox;

        return $this;
    }

    /**
     * Get count_in_box
     *
     * @return integer
     */
    public function getCountInBox()
    {
        return $this->count_in_box;
    }

    /**
     * Set size
     *
     * @param string $size
     * @return PageParsed
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return string
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return PageParsed
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return PageParsed
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set page
     *
     * @param \S2b\CrawlerBundle\Entity\Page $page
     * @return PageParsed
     */
    public function setPage(\S2b\CrawlerBundle\Entity\Page $page = null)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get page
     *
     * @return \S2b\CrawlerBundle\Entity\Page
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return PageParsed
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set photo_url
     *
     * @param string $photoUrl
     * @return PageParsed
     */
    public function setPhotoUrl($photoUrl)
    {
        $this->photo_url = $photoUrl;

        return $this;
    }

    /**
     * Get photo_url
     *
     * @return string 
     */
    public function getPhotoUrl()
    {
        return $this->photo_url;
    }

    /**
     * Set sku
     *
     * @param string $sku
     * @return PageParsed
     */
    public function setSku($sku)
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * Get sku
     *
     * @return string 
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * Set downloadedAt
     *
     * @param \DateTime $downloadedAt
     * @return PageParsed
     */
    public function setDownloadedAt($downloadedAt)
    {
        $this->downloadedAt = $downloadedAt;

        return $this;
    }

    /**
     * Get downloadedAt
     *
     * @return \DateTime 
     */
    public function getDownloadedAt()
    {
        return $this->downloadedAt;
    }
}
