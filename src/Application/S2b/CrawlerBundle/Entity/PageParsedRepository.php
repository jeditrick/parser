<?php

namespace Application\S2b\CrawlerBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * 
 */
class PageParsedRepository extends EntityRepository
{
    
    /**
     * 
     */
    public function findChunked($chunk, $offset = 0) {
        return $this
            ->createQueryBuilder('pp')
                ->setFirstResult($offset)
                ->setMaxResults($chunk)
                ->getQuery()
                    ->getResult();
    }

    /**
     * 
     */
    public function countPagesParsed() {
        return $this
            ->createQueryBuilder('pp')
            ->select('count(pp.id)')
                ->getQuery()
                    ->getSingleScalarResult();
    }

    public function findOneBySku($sku)
    {
        return $this
            ->createQueryBuilder('p')
            ->where("p.sku = :sku")
            ->setParameter('sku', $sku)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findOneNotDownloaded()
    {
        return $this
            ->createQueryBuilder('p')
            ->where("p.downloadedAt IS NULL")
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

}
