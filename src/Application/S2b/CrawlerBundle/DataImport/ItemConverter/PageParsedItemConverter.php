<?php

namespace Application\S2b\CrawlerBundle\DataImport\ItemConverter;

use Ddeboer\DataImport\ItemConverter\ItemConverterInterface;

class PageParsedItemConverter implements ItemConverterInterface
{
    /**
     * {@inheritDoc}
     */
    public function convert($input)
    {
        $input['original_number'] = $this->prepareOriginalNumber($input['original_number']);
        return $input;
    }

    /**
     *
     */
    protected function prepareOriginalNumber ($originalNumber) {

        $originalNumber = str_replace('Mazda', '', $originalNumber);
        $originalNumber = trim($originalNumber);

        return $originalNumber;
    }
}
