<?php

namespace Application\S2b\CrawlerBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\LockHandler;

class RemoveCategoriesCommand extends ContainerAwareCommand
{


    public function removeRelatedEntities()
    {

    }

    protected function configure()
    {
        $this
            ->setName('s2b:crawler:remove-categories')
            ->setDescription('Remove product categories from DB');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $lockHandler = new LockHandler($this->getName());
        if (!$lockHandler->lock()) {
            $output->writeln("<error>Remove categories already running</error>");

            return 0;
        }

        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();

        $pageRepository = $doctrine->getRepository('S2bCrawlerBundle:Page');


        $categoryPages = $pageRepository->createQueryBuilder('p')
            ->where('p.url LIKE :categories ')
            ->setParameter('categories', '%categories%')
            ->getQuery()
            ->getResult();
        foreach ($categoryPages as $category) {
            if($category->getCrawled() != null){
                $em->remove($category->getCrawled());
            }
            $em->remove($category);
        }
        $em->flush();
        $output->writeln('Categories removed');
        return true;
    }

}
