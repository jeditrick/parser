<?php

namespace Application\S2b\CrawlerBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Output\ConsoleOutput;

use Ddeboer\DataImport\Workflow;
use Ddeboer\DataImport\Writer\ExcelWriter;
use Ddeboer\DataImport\ValueConverter\CallbackValueConverter;
// use Ddeboer\DataImport\Writer\ConsoleProgressWriter;
use Ddeboer\DataImport\Reader\DbalReader;

use Application\S2b\CrawlerBundle\DataImport\ItemConverter\PageParsedItemConverter;

class ExportCommand extends ContainerAwareCommand
{

    /**
     * 
     */
    protected function configure()
    {
        $this
            ->setName('s2b:parser:export')
            ->setDescription('Export last [limit] parsed records to file with proper [format]')
            ->addArgument(
                'filename', 
                InputArgument::REQUIRED, 
                'Name of file'
            )
            ->addArgument(
                'from', 
                InputArgument::OPTIONAL, 
                'From ID',
                1
            )
            ->addArgument(
                'to', 
                InputArgument::OPTIONAL, 
                'To ID',
                0
            )
            ->addOption(
                'formats', 
                null, 
                InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY,
                'File format', 
                array('xls')
            )
        ;
    }

    /**
     * 
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filename = $input->getArgument('filename');
        $formats = $input->getOption('formats');
        $from = $input->getArgument('from');
        $to = $input->getArgument('to');

        $where = '';
        if ($from) $where .= ($where ? ' and ' : '') . "'$from' <= id";
        if ($to) $where .= ($where ? ' and ' : '') . "id <= '$to'";
        if ($where) $where = "where $where";

        $dbal = $this->getContainer()->get('database_connection');
        $reader = new DbalReader($dbal, "SELECT id, page_id, original_number, internal_number, model, name, price, price_currency_code, description FROM `s2b_crawler_page_parsed` $where");
        // $progressWriter = new ConsoleProgressWriter(new ConsoleOutput(), $reader);
        $workflow = new Workflow($reader);

        foreach ($formats as $format) {
            switch ($format) {
                case 'xls':
                    $writer = new ExcelWriter(new \SplFileObject('./web/' . $filename, 'w'));
                    break;

                default:
                    $output->writeln("<error>Unsupported format '$format'</error>");
                    return;
            }
            $workflow->addWriter($writer);
        }

        $result = $workflow
            // ->addWriter($progressWriter)
            ->addItemConverter(new PageParsedItemConverter())
            ->setSkipItemOnFailure(true)
            ->process()
        ;

        $output->writeln('Export finished');
        $output->writeln('Elapsed time: ' . $result->getElapsed()->format('%s seconds'));
        $output->writeln('Success: ' . $result->getSuccessCount());
        $output->writeln('Errors: ' . $result->getErrorCount());
    }
}
