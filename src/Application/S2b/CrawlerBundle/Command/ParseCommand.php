<?php

namespace Application\S2b\CrawlerBundle\Command;

use Application\S2b\CrawlerBundle\Entity\PageParsed;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;

class ParseCommand extends ContainerAwareCommand
{
    /**
     * 
     */
    protected function configure()
    {
        $this
            ->setName('s2b:parser:parse')
            ->setDescription('Parse single page with given [id]')
            ->addArgument('id', InputArgument::REQUIRED, 'Page Id to parse')
        ;
    }

    /**
     * 
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();
        $pageRepository = $doctrine->getRepository('S2bCrawlerBundle:Page');
        $pageParsedRepository = $doctrine->getRepository('ApplicationS2bCrawlerBundle:PageParsed');
        $id = $input->getArgument('id');

        $page = $pageRepository->findOneById($id);

        if (!$page) {
            $output->writeln("<error>Page not found</error>");
            return;
        }

        if (!$page->isCrawled()) {
            $output->writeln("<error>Page not crawled</error>");
            return;
        }

        if ($page->isParsed()) {
            $output->writeln("<error>Page already parsed</error>");
            return;
        }
        
        if (!$page->getCrawled()) {
            
            $page->setParsedAt(new \DateTime());
            $em->persist($page);

            $em->flush();
            return;
        }

        // Parse
        $crawler = new Crawler($page->getCrawled()->getContent(), $page->getUrl());
        $parsed = new PageParsed();

        $offers = $crawler->filter('.article');

        if (count($offers) && strpos($page->getUrl(),'items')) {
            $parsed
                ->setPage($page)
                ->setName(trim($offers->filter('.pic + h2')->text()))
                ->setPricePerOne(
                    (int)preg_replace(
                        '/[^\d]/',
                        '',
                        trim($offers->filter('.price > span:nth-child(2) > b > span')->text()))
                )
                ->setCountInBox(
                    (int)preg_replace(
                        '/[^\d]/',
                        '',
                        trim($offers->filter('.price > span:nth-child(4) > b')->text()))
                )
                ->setSize(
                    preg_replace(
                        '/[^\d-]/',
                        '',
                        trim($offers->filter('div.price > b:nth-child(6)')->text()))

                )
                ->setPrice(
                    (int)preg_replace(
                        '/[^\d]/',
                        '',
                        trim($offers->filter('.price > b:nth-child(8) > span')->text()))
                )->setSku(
                    preg_replace(
                        '/.+\s/',
                        '',
                        trim($offers->filter('.price > b:nth-child(7)')->text()))
                )->setPhotoUrl(
                    trim($offers->filter('#pic > a > img')->attr('src'))
                )->setCreatedAt(
                    new \DateTime()
                );

            if ($output->isVeryVerbose()) {
                $output->writeln("Parsed price per one #" . $parsed->getPricePerOne());
                $output->writeln("Parsed count in box #" . $parsed->getCountInBox());
                $output->writeln("Parsed size #" . $parsed->getSize());
                $output->writeln("Parsed price #" . $parsed->getPrice());
                $output->writeln("Parsed sku #" . $parsed->getSku());
                $output->writeln("Parsed photo url #" . $parsed->getPhotoUrl());

            }
            if ($pageParsedRepository->findBySku($parsed->getSku()) == null) {
                $page->setParsed($parsed);
                $em->persist($parsed);
            } else {
                $output->writeln('This SKU ( ' . $parsed->getSku() . ' ) has already exist in DB ');
                return;
            }
        }

        $page->setParsedAt(new \DateTime());
        $em->persist($page);

        $em->flush();

        $output->writeln('Parsed ' . $page->getUrl());
    }
}
