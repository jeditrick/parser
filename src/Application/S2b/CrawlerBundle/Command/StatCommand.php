<?php

namespace Application\S2b\CrawlerBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class StatCommand extends ContainerAwareCommand
{
    /**
     * 
     */
    protected function configure()
    {
        $this
            ->setName('s2b:parser:stat')
            ->setDescription('Shows statistic about parsed / crawled pages')
        ;
    }

    /**
     * 
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();
        
        $pageRepository = $doctrine->getRepository('S2bCrawlerBundle:Page');
        $pageCrawledRepository = $doctrine->getRepository('S2bCrawlerBundle:PageCrawled');
        $pageParsedRepository = $doctrine->getRepository('ApplicationS2bCrawlerBundle:PageParsed');

        $queue = $pageRepository->countPages();
        $crawled = $pageCrawledRepository->countPagesCrawled();
        $parsed = $pageParsedRepository->countPagesParsed();

        $output->writeln('Parsed / Crawled / Queue');
        $output->writeln("$parsed / $crawled / $queue");
    }
}
