<?php

namespace Application\S2b\CrawlerBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\LockHandler;

class PopCommand extends ContainerAwareCommand
{

    /**
     * 
     */
    protected function configure()
    {
        $this
            ->setName('s2b:parser:pop')
            ->setDescription('Parse last [limit] unparsed pages from queue')
            ->addOption('limit', null, InputOption::VALUE_REQUIRED, 'Number of Pages to parse', 10)
        ;
    }

    /**
     * 
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $lockHandler = new LockHandler('parser-pop.lock');
        if (!$lockHandler->lock()) {
            $output->writeln("<error>Parser already running</error>");

            return 0;
        }

        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();
        $pageRepository = $doctrine->getRepository('S2bCrawlerBundle:Page');

        $limit = $input->getOption('limit');

        do {
            $page = $pageRepository->findOneNotParsed();

            if (!$page) {
                $output->writeln("<error>Empty queue</error>");
                return;
            }

            $output->writeln('Found ' . $page->getUrl());
            
            $input = [
                'command'=>'s2b:parser:parse',
                'id'=>$page->getId(),
                '--verbose'=>$output->getVerbosity()
            ];

            if ($output->isVerbose()) {
                $output->writeln('Running ' . $input['command'] . ' with id ' . $input['id']);
            }

            $this->getApplication()
                ->find('s2b:parser:parse')
                ->run(new ArrayInput($input), $output);

            $output->writeln('');

        } while (--$limit > 0);

        $output->writeln('Parsing finished');
    }
}
