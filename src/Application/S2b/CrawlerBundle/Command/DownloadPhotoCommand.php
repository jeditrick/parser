<?php

namespace Application\S2b\CrawlerBundle\Command;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\LockHandler;

class DownloadPhotoCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('s2b:download:photo')
            ->setDescription('Donwload photo from site');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $lockHandler = new LockHandler($this->getName());
        if (!$lockHandler->lock()) {
            $output->writeln("<error>This command is running</error>");

            return 0;
        }

        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();
        $fs = new Filesystem();
        $client = new Client();

        $parsedPage = $doctrine->getRepository('ApplicationS2bCrawlerBundle:PageParsed')
            ->findOneNotDownloaded();
        if (!$parsedPage) {
            $output->writeln("<error>Empty queue</error>");
            return;
        }

        try {
            $web_path = sprintf(
                "%s/../web/downloaded",
                $this->getContainer()->getParameter('kernel.root_dir')
            );
            $photo_path = sprintf(
                "%s/Обувь Fontan Obuvi модель %s %sUAH %s, Размеры %s.jpg",
                $web_path,
                $parsedPage->getName(),
                $parsedPage->getPricePerOne(),
                $parsedPage->getCountInBox(),
                $parsedPage->getSize()
            );
            if (!$fs->exists($web_path)) {
                $fs->mkdir($web_path);
            }
            try {
                $fs->dumpFile($photo_path, $client->get($parsedPage->getPhotoUrl())->getBody()->getContents());
                if ($fs->exists($photo_path)) {
                    $parsedPage->setDownloadedAt(new \DateTime());
                    $em->persist($parsedPage);
                    $output->writeln('Photo downloaded');
                } else {
                    $output->writeln("Photo hasn't downloaded");
                }
            } catch (ClientException $e) {
                $parsedPage->setDownloadedAt(new \DateTime());
                $em->persist($parsedPage);
                $output->writeln(
                    sprintf("<error>Photo for item %s (%s), doesn't downloaded, 404 error</error>",
                        $parsedPage->getName(),
                        $parsedPage->getPhotoUrl()
                    )
                );
            }

        } catch (IOExceptionInterface $e) {
            echo "An error occurred while creating your directory at " . $e->getPath();
        }
        $em->flush();
        return true;
    }

}
