<?php

    namespace Application\S2b\CrawlerBundle\Command;

    use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
    use Symfony\Component\Console\Input\InputInterface;
    use Symfony\Component\Console\Output\OutputInterface;
    use Symfony\Component\Filesystem\LockHandler;
    use Symfony\Component\Finder\Finder;

    class CheckPhotoNameCommand extends ContainerAwareCommand
    {

        protected function configure()
        {
            $this
                ->setName('s2b:check:photo-name')
                ->setDescription('Check downloaded photo name');
        }

        protected function execute(InputInterface $input, OutputInterface $output)
        {
            $lockHandler = new LockHandler($this->getName());
            if (!$lockHandler->lock()) {
                $output->writeln("<error>Remove categories already running</error>");

                return 0;
            }
            $finder = new Finder();
            $web_path = sprintf(
                "%s/../web/downloaded",
                $this->getContainer()->getParameter('kernel.root_dir')
            );
            $finder->files()->in($web_path);

            foreach ($finder as $file) {
                if (
                preg_match(
                    '/
                    (?P<good_name>.+)							# Имя товара
					\s
					(?P<firm_name>[^\s]+)						# Фирма
					\s+модель\s
					(?P<good_model>.+)							# Номер модели - все что до цифры
					\s
					(?P<good_source_price>[\d\.]+)				# Цифра
					(?P<good_source_price_currency>[A-Z]{3})	# Валюта
					\s
					(?P<good_wholesale_min_count>\d+) 			# В ящике
					(\,(?P<good_comment>.+))?					# Комментарий через запятую
					/mix',
                    $file->getRelativePathname()
                )
                ) {
                    $output->writeln(
                        sprintf(
                            "<info>Passed: %s</info>",
                            $file->getRelativePathname()
                        )
                    );
                } else {
                    $output->writeln(
                        sprintf(
                            "<error>Not passed: %s</error>",
                            $file->getRelativePathname()
                        )
                    );
                }

            }

            return true;
        }

    }
